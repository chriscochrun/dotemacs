(defvar org-bible-directory "~/docs/bibles")
(defvar org-bible-books '("genesis" "exodus" "leviticus" "numbers" "dueteronomy" "joshua" "judges" "ruth" "1-samuel" "2-samuel" "1-kings" "2-kings" "1-chronicles" "2-chronicles" "ezra" "nehemiah" "esther" "job" "psalms" "proverbs" "ecclesiastes" "song-of-solomon" "isaiah" "jeremiah" "lamentations" "ezekiel" "daniel" "hosea" "joel" "amos" "obadiah" "jonah" "micah" "nahum" "habakkuk" "zephaniah" "haggai" "zechariah" "malachi" "matthew" "mark" "luke" "john" "acts" "romans" "1-corinthians" "2-corinthians" "galatians" "ephesians" "philipians" "colossians" "1-thessalonians" "2-thessalonians" "1-timothy" "2-timothy" "titus" "philemon" "hebrews" "james" "1-peter" "2-peter" "1-john" "2-john" "3-john" "jude" "revelation"))

(defvar org-bible-default "~/docs/bibles/esv.org")
(defvar org-bible-selected org-bible-default)

(defun org-bible-find-verse-imenu ()
  "Using Imenu find the verse and jump to it using the default bible"
  (interactive)
  (find-file org-bible-selected)
  (let ((location (list (imenu-choose-buffer-index))))
    (imenu-default-goto-function nil (cdr (car location)))))

(defun org-bible-chapters (book)
  "Get the chapters of the book in a list"
  (save-excursion
    (let ((book (org-bible-book-find book)))
      (org-bible-children book))))

(defun org-bible-verses (chapter book)
  "Get the verses of the chapter in a list"
  (save-excursion
    (let ((chapter (org-bible-chapter-find chapter book)))
      (message "chapter: %s" chapter)
      (org-bible-children chapter))))

(defun org-bible-children (item)
  "Get the children of the current item in the form of a point"
  (save-excursion
    (org-with-wide-buffer
     (goto-char item)
     (when (org-goto-first-child)
       (cl-loop collect (org-get-heading t t)
                while (outline-get-next-sibling))))))

(defun org-bible-chapter-find (chapter book)
  (let ((book (concat "* " (string-replace "-" " " book))))
    (org-with-wide-buffer
     (save-excursion
       (widen)
       (goto-char 1)
       (search-forward book)
       (search-forward chapter)
       (point)))))

(defun org-bible-verse-find (verse chapter book)
  (let ((book (concat "* " (string-replace "-" " " book))))
    (org-with-wide-buffer
     (save-excursion
       (widen)
       (goto-char 1)
       (search-forward book)
       (search-forward chapter)
       (search-forward verse)
       (point)))))

(defun org-bible-book-find (book)
  (let ((book (concat "* " (string-replace "-" " " book))))
    (org-with-wide-buffer
     (save-excursion (widen)
                     (goto-char 1)
                     (search-forward book)
                     (point)))))
      

(defun org-bible-jump ()
  "Jump to passage"
  (interactive)
  (with-current-buffer (find-file org-bible-selected)
    (let* ((book (completing-read "Book: " org-bible-books))
           (chapter (completing-read "Chapter: " (org-bible-chapters book)))
           (verse (completing-read "Verse: " (org-bible-verses chapter book))))
      (goto-char (org-bible-verse-find verse chapter book))
      (beginning-of-line)
      (pulsar-recenter-top))))

(defun org-bible-select ()
  "Pick a different bible that is formatted in org-mode"
  (interactive)
  (let ((bible (completing-read "Pick a bible version: " '("ESV" "CSB" "NASB"))))))

;; (define-minor-mode org-bible-mode
;;   "Toggles the org bible minor mode"
;;   nil
;;   :global t
;;   :lighter " bible")
